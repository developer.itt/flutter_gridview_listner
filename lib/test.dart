import 'dart:math';

import 'package:flutter/material.dart';

class TestScaffold extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TestScaffoldState();
}

List<_SquareButton> _selectedList = [];
List<_SquareButton> temp_selectedList = [];

String str = '', str2 = '';
const _chars = 'ABCD';
Random _rnd = Random();
bool ismatched = false;
String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

class _TestScaffoldState extends State<TestScaffold> {
  List<_SquareButton> buttons = [];
  @override
  void initState() {
    super.initState();

    for (int i = 1; i <= 25; i++) {
      buttons.add(_SquareButton(getRandomString(1)));
    }
  }

  Future<void> cleardata() async {
    setState(() {
      // ignore: invalid_use_of_protected_member
      (context as Element).reassemble();
      str = '';
      str2 = '';
      _selectedList.clear();
      buttons.clear();
      for (int i = 1; i <= 25; i++) {
        buttons.add(_SquareButton(getRandomString(1)));
      }
    });
  }

  Map<Rect, _SquareButton> positions = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('TouchListner Demo'),
        ),
        body: Column(
          children: [
            Container(
              alignment: Alignment.center,
              child: GestureDetector(
                onPanDown: (details) {
                  temp_selectedList.clear();
                  temp_selectedList.addAll(_selectedList);
                  _selectedList.clear();
                  checkGesture(details.globalPosition);
                  (context as Element).reassemble();
                },
                onPanUpdate: (details) {
                  checkGesture(details.globalPosition);
                },
                onPanEnd: (detail) {
                  setState(() {
                    str2 = '';
                    str2 = str;
                    str = '';
                    str2 == 'AAA' ? ismatched = true : ismatched = false;
                    (context as Element).reassemble();
                  });
                },
                child: GridView.count(
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 5,
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  children: buttons,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(child: Center(child: Text(str2))),
            ),
            ElevatedButton(
              onPressed: cleardata,
              child: const Text('Clear'),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Container(
                height: 50,
                width: 150,
                color: ismatched == false ? Colors.red : Colors.green,
                child: const Center(child: Text('AAA')),
              ),
            )
          ],
        ));
  }

  initPositions() {
    if (positions.isNotEmpty) return;
    buttons.forEach((btn) {
      RenderBox box = btn.bKey.currentContext?.findRenderObject() as RenderBox;
      Offset start = box.localToGlobal(Offset.zero);
      Rect rect =
          Rect.fromLTWH(start.dx, start.dy, box.size.width, box.size.height);
      positions.addAll({rect: btn});
    });
  }

  checkGesture(Offset position) {
    initPositions();
    positions.forEach((rect, btn) {
      if (rect.contains(position)) {
        if (!_selectedList.contains(btn)) {
          _selectedList.add(btn);
          setState(() {
            str = str + btn.title;
          });
          btn.state.setState(() {});
        }
      }
    });
  }
}

class _SquareButton extends StatefulWidget {
  final String title;
  _SquareButton(this.title);

  final GlobalKey bKey = GlobalKey();
  late State state;
  @override
  // ignore: no_logic_in_create_state
  State<StatefulWidget> createState() {
    state = _SquareButtonState();
    return state;
  }
}

class _SquareButtonState extends State<_SquareButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      key: widget.bKey,
      padding: EdgeInsets.all(5.0),
      child: Container(
        color: _selectedList.contains(widget) ? Colors.tealAccent : Colors.teal,
        child: Text(widget.title),
        alignment: Alignment.center,
      ),
    );
  }
}
