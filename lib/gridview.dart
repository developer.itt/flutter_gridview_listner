import 'package:flutter/material.dart';

class Gridview extends StatefulWidget {
  Gridview({Key? key}) : super(key: key);

  @override
  State<Gridview> createState() => _GridviewState();
}

class _GridviewState extends State<Gridview> {
  @override
  Widget build(BuildContext context) {
    final double itemHeight = MediaQuery.of(context).size.width / 10;
    final double itemWidth = MediaQuery.of(context).size.width / 10;
    
    return Scaffold(
      body: GestureDetector(

        child: Container(
          alignment: Alignment.center,
          color: Colors.red,
          padding: const EdgeInsets.only(left: 6, right: 6),
          child: GridView.count(
            childAspectRatio: (itemWidth / itemHeight),
            crossAxisCount: 10,
            crossAxisSpacing: 3,
            mainAxisSpacing: 3,
            children: List.generate(
              100,
              (index) {
                return ElevatedButton( 
                    onPressed: () {
                      print("tap");
                    },
                    child: Center(child: Text('$index')));
              },
            ),
          ),
        ),
      ),
    );
  }
}
